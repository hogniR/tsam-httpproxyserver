import SocketServer
import logging
import time
import select
import socket

from http import *
from cache import *


proxy_state = None


class HTTPProxyServer(object):
    """
    Base Threaded HTTPProxy server.

    """

    def __init__(self, init_state):
        global proxy_state
        proxy_state = init_state
        self.proxyserver_host = proxy_state.listenaddress
        self.proxyserver_port = proxy_state.listenport
        self.proxyserver = None

    def startproxyserver(self):
        """
        Starts threaded proxyserver. Sets main (initial) thread as Daemon thread.
        :return: None
        """
        global proxy_state

        self.proxyserver = ThreadedHTTPProxyServer((self.proxyserver_host, self.proxyserver_port), ProxyRequestHandler)

        server_thread = threading.Thread(target=self.proxyserver.serve_forever)

        server_thread.setDaemon(True)
        proxy_state.log.info("Server %s listening on port %d", self.proxyserver_host, self.proxyserver_port)
        # starts main thread, spawns a new thread for each connection.
        server_thread.start()

        while True:
            #time between thread polls, for shutdown (No timeout causes lockup)
            time.sleep(0.1)

    def stopproxyserver(self):
        """
        Shuts down proxy server 'safely'
        :return: None
        """
        self.proxyserver.shutdown()


class ThreadedHTTPProxyServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer, object):
    """
    Threaded HTTP server setup, allows use of same network address for multiple connections
    """
    # Overrides TCPServer default for reusing addresses
    allow_reuse_address = True


class ProxyState(object):
    """
    Object to hold state of the proxy
    """

    def __init__(self, port=8080, addr='localhost', cacheroot=None):
        self.listenport = port
        self.listenaddress = addr
        if cacheroot is not None:
            self.cache = Cache(cacheroot)
        else:
            self.cache = Cache()

        # uses default module level logger,
        #No need to worry about thread safety, built in to python logging
        self.log = logging.getLogger(__name__)

    @staticmethod
    def gettargethost(req):
        """
        Returns target address for HTTPRequest
        :param req: Request containing host
        :return:
        """

        # sometimes raises assertions errors, seems to not receive req
        global proxy_state
        proxy_state.log.debug('getting host for %s', req)
        return req.get_host()


class ProxyRequestHandler(SocketServer.StreamRequestHandler, object):
    """
    Handler for each proxy request
    """

    def __init__(self, request, client_address, server):
        self.keepalive = False
        self.target = None
        self._host = None
        self._port = 0
        # Initialises with StreamRequestHandler initializer
        super(ProxyRequestHandler, self).__init__(request, client_address, server)

    def createconnection(self, host, port):
        """
        Creates connection to external host (target web server)
        :param host: Host to connect to
        :param port: port number to connect to (default:80)
        :return: HTTPConnection for the connection
        """
        global proxy_state
        conn = None
        if self.target and self._host == host:
            return self.target
        try:
            # use httplib here for now.
            proxy_state.log.debug('trying to connect to %s,%d', host, port)
            # connects to remote host
            conn = socket.create_connection((host, port))
        except socket.error as e:
            proxy_state.log.debug('91: trying to connect to %s, %d', host, port)

        if self.keepalive:
            # connection is persistent
            self.target = conn

        self._host = host
        self._port = port

        return conn

    def sendresponse(self, resp):
        """
        Sends response to client
        :param resp: serialized response
        :return: None
        """
        self.wfile.write(resp)

    def finish(self):
        """
        Ends handling, closes an open HTTPConnection
        :return: handled request
        """
        if not self.keepalive:
            if self.target:
                self.target.close()
            return super(ProxyRequestHandler, self).finish()
        return self.handle()

    def handle(self):
        """
        Waits to read a socket if keep alive is set.
        Tries to build request from client input.
        Tries to perform HTTP method with request
        :return: None (early return if no request)
        """
        global proxy_state
        res = None

        if self.keepalive:
            select.select([self.request], [], [])

        try:
            req = HTTPRequest.build(self.rfile)
        except Exception as e:
            proxy_state.log.error('%s : Error on reading request', e)
            return

        if req is None:
            return

        # if request states to keep alive set handler keepalive attribute
        self.keepalive = req.is_keep_alive()

        #log for error here
        host, port = ProxyState.gettargethost(req)
        req.add_header("Via", "The Proxy")

        if proxy_state.cache.iscached(req):
            res = proxy_state.cache.getresp(req)
        else:
            #decide on request method which sending method to use
            if (req.method == "GET") or (req.method == "POST") or (req.method == "HEAD"):
                conn = self.createconnection(host, port)
                serverwfile = conn.makefile("w + b")
                serverwfile.write(req.serialize())
                serverwfile.close()
                serverwfile = None
                select.select([self.target], [], [])
                serverrfile = conn.makefile("r + b")
                res = HTTPResponse.build(serverrfile)
                serverrfile.close()
                # if cache-control valid

                cachecontrol = res.get_header("cache-control")
                if cachecontrol != []:
                    cachecontrol = cachecontrol[0].split(",")
                    for i in cachecontrol:
                        if i.lower().strip() == "private":
                            res.cacheable = False
                            break
                        elif i.lower().strip() == "no-cache":
                            res.cacheable = False
                            break
                        elif i.lower().strip() == "proxy-revalidate":
                            res.cacheable = False
                        elif i.lower().startswith("max-age"):
                            res.expiry = res.expiry + datetime.timedelta(seconds=(int(i.split("=")[1])))

                if res.cacheable:
                    proxy_state.cache.cache(req, res, res.expiry)

                self.sendresponse(res.serialize())

        proxy_state.log.info("%s:%d %s %s %s %s",
                             self.client_address[0],
                             self.client_address[1],
                             req.method,
                             req.get_path(),
                             res.code,
                             res.msg)
