import threading
import hashlib
import datetime
import pickle
import os
import shutil


class Cache(object):
    def __init__(self, cache_root="cache/"):
        self.cached = {}
        self.root_path = cache_root
        if os.path.exists(cache_root):
            shutil.rmtree(cache_root, ignore_errors=True)

        os.mkdir(cache_root)

    def iscached(self, request):
        try:
            now = datetime.datetime.now()
            expiry_time = self.cached[request.url]['Expiry']
            if expiry_time > now:
                return True
        finally:
            return False

    def cache(self, request, response, expiry):
        filename = hashlib.md5(request.url).hexdigest()
        self.cached[request.url] = {"Filename": filename, "Expiry": expiry}
        lock = threading.Lock()
        lock.acquire()
        try:
            path = os.path.abspath(self.root_path + filename)
            fd = open(path, "w+b")
            pickle.dump(response, fd)
            fd.close()
        finally:
            lock.release()
        pass

    def retrieve(self, request):

        filename = hashlib.md5(request.url).hexdigest()
        lock = threading.Lock()
        lock.acquire()
        try:
            path = os.path.abspath(self.root_path + filename)
            fd = open(path, "r+b")
            resp = pickle.load(fd)
            fd.close()
        finally:
            lock.release()

        return resp