"""
HTTP Proxy server
Implements GET, POST, HEAD
Implements Threading for pipelining
Implements caching using md5 mapping to filename
"""
import argparse

from HTTPProxy import *

# initialise parser for parsing command line arguments
parser = argparse.ArgumentParser(description='Start an HTTP Proxy Server')

# add positional arguments to parser
parser.add_argument('listenport',
                    help='Port number on which to start the proxy server.',
                    type=int)
parser.add_argument('logfile',
                    help='File name in which to store the log data.',
                    type=str,
                    default='pythonproxyserver.log')

parser.add_argument('-a', '--listenaddress',
                    help='Address on which to listen for incoming connections',
                    type=str)

# add optional arguments to specify debug mode
parser.add_argument('-d', '--debug',
                    help='Turn on debug mode (increases volume of output to logfile).',
                    action='store_true')

parser.add_argument('-c', '--cacheroot',
                    help='Changes the cacheroot to this root, relative to the directory the script is run in.',
                    type=str)


def parse_args():
    args = parser.parse_args()
    ps = ProxyState()

    date_formatter = '%Y-%m-%dT%H:%M:%S%z'
    logging_formatter = '%(asctime)s : %(message)s'
    os.remove(args.logfile)
    if args.debug:
        print 'Debug set'
        logging.basicConfig(level=logging.DEBUG,
                            format=logging_formatter,
                            filename=args.logfile,
                            datefmt=date_formatter)
    else:
        print 'Debug not set'
        logging.basicConfig(level=logging.INFO,
                            format=logging_formatter,
                            filename=args.logfile,
                            datefmt=date_formatter)
    ps.log.debug('Messages for everyone')

    ps.listenport = args.listenport

    if args.listenaddress is not None:
        ps.listenaddress = args.listenaddress

    return ps


def main():
    """
    Parses command line arguments, then starts proxyserver on specified address and port.
    'Should' close on keyboard interrupt
    :return: None
    """
    global proxy_state

    proxy_state = parse_args()
    proxy_state.log.info('Starting Server on %s:%d', proxy_state.listenaddress, proxy_state.listenport)
    proxy = HTTPProxyServer(proxy_state)
    try:
        proxy.startproxyserver()
    except KeyboardInterrupt:
        print 'hit error'
        proxy.stopproxyserver()


if __name__ == '__main__':
    # If run as main module, runs main and computes proxy uptime
    starttime = datetime.datetime.now()
    global proxy_state
    main()
    endtime = datetime.datetime.now()
    uptime = endtime - starttime
    proxy_state.log.info('Proxy closing. Served for %s', uptime)

