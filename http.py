import datetime
import urlparse


class HTTPMessage(object):
    """
    Parent class for all HTTP messages
    """
    EOL = "\r\n"
    HTTP_CODE_OK = 200

    def __init__(self, headers=None):
        """
        Initialises message with headers
        :param headers: iterable that contains headers
        :return: None
        """

        # Checks different formats for headers (could be list, dict or None(empty)) and fixes to dict format
        if headers is None:
            self.headers = {}
        elif isinstance(headers, list):
            self.headers = HTTPMessage._read_headers(headers)
        else:
            assert isinstance(headers, dict)
            self.headers = headers

    @staticmethod
    def _read_headers(data):
        """
        Reads headers to dict format.
        :param data: Header information
        :return: dictionary of headers
        """
        headers = {}

        for line in data:
            if line == HTTPMessage.EOL:
                break
            assert ":" in line
            line = line.rstrip(HTTPMessage.EOL)
            i = line.index(":")
            n = line[:i]
            v = line[i + 1:]
            if n not in headers:
                headers[n] = []
            headers[n].append(v.lstrip())

        return headers

    @staticmethod
    def _read_body(data, headers):
        """
        Determines read length by content length header field. Check whether data is chunked.
        reads body and returns
        :param data: HTTP Message to read
        :param headers: Header dictionary
        :return: body of http message, even if chunked.
        """
        body_length = None
        chunked = False
        for n, v in headers.iteritems():
            if n.lower() == "content-length":
                body_length = int(v[0])
            elif n.lower() == "transfer-encoding" and v[0].lower() == "chunked":
                chunked = True
                break

        # Read HTTP body (if present)
        body = ""
        if body_length is not None:
            body = data.read(body_length)
        elif chunked:
            # Chunked encoding
            while True:
                # Determine chunk length
                chunk_length = data.readline()
                chunk_length = int(chunk_length, 16)

                # Read the whole chunk
                chunk = data.read(chunk_length)
                body += chunk

                if chunk_length == 0:
                    break

                # Read trailing CRLF
                eol = data.read(2)

        return body

    def is_chunked(self):
        """
        Checks headers to see if chunked mode set
        :return: True is message is chunked
        """
        r = False
        for n, v in self.headers.iteritems():
            if n.lower() == "transfer-encoding" and v[0].lower() == "chunked":
                r = True
                break
        return r

    def is_keep_alive(self):
        """
        checks headers to see if connection should be persistent
        :return: True if connection is persistent
        """
        if 'Connection' in self.headers:
            if self.headers['Connection'][0] == 'keep-alive':
                return True
        elif 'Proxy-Connection' in self.headers:
            if self.headers['Proxy-Connection'][0] == 'keep-alive':
                return True
        return False

    def __find_header(self, name, ignorecase=True):
        """
        Checks to see if header exists
        :param name: name of header searched for
        :param ignorecase: if True ignores case of header fieldname
        :return: returns header field name and value
        """
        r = None
        for n in self.headers:
            if (ignorecase and name.lower() == n.lower()) or ((not ignorecase) and name == n):
                r = n
                break
        return r

    def get_header(self, name, ignorecase=True):
        r = []
        for n, v in self.headers.iteritems():
            if (ignorecase and name.lower() == n.lower()) or ((not ignorecase) and name == n):
                r.extend(v)
        return r

    def add_header(self, name, value, ignorecase=True):
        k = self.__find_header(name, ignorecase)
        if k not in self.headers:
            self.headers[name] = []
        self.headers[name].append(value)


class HTTPRequest(HTTPMessage, object):
    """
    class for HTTP requests
    """

    def __init__(self, method, url, protocol, headers=None, body=''):
        self.method = method
        self.url = url
        self.protocol = protocol
        self.body = body
        HTTPMessage.__init__(self, headers)

    @staticmethod
    def build(data):
        """
        Builds HTTP request from data
        :param data: data from which to build http request
        :return: HTTP request
        """
        # Read request line
        reqline = data.readline().rstrip(HTTPMessage.EOL)

        if reqline == '':
            return None

        method, url, proto = reqline.split()

        # Read headers & body
        headers = HTTPMessage._read_headers(data)
        body = HTTPMessage._read_body(data, headers)
        return HTTPRequest(method, url, proto, headers, body)

    def serialize(self):
        """
        Converts HTTP response method to string
        :return:
        """
        # Response line
        s = "%s %s %s" % (self.method, self.url, self.protocol)
        s += HTTPMessage.EOL

        # Headers
        for n, v in self.headers.iteritems():
            for i in v:
                s += "%s: %s" % (n, i)
                s += HTTPMessage.EOL

        s += HTTPMessage.EOL

        # Body
        if not self.is_chunked():
            s += self.body
        else:
            s += "%x" % len(self.body) + HTTPMessage.EOL
            s += self.body + HTTPMessage.EOL
            s += HTTPMessage.EOL
            s += "0" + HTTPMessage.EOL + HTTPMessage.EOL

        return s

    def get_host(self):
        """
        gets target host of HTTP request
        :return: address of target
        """
        r = urlparse.urlparse(self.url)
        port = r.port or 80

        host = r.hostname

        return host, port

    def get_path(self):
        """
        Parses the url path for proper format in request
        :return: fully parsed and reformatted path
        """
        r = urlparse.urlparse(self.url)
        s = r.path
        if len(r.params) > 0:
            s += ';%s' % r.params
        if len(r.query) > 0:
            s += '?%s' % r.query
        if len(r.fragment) > 0:
            s += '#%s' % r.fragment

        return s

    def get_params(self):
        """
        gets parameters from POST request
        :return:
        """
        params = {}

        if len(self.body) > 0:
            params.update(urlparse.parse_qs(self.body, keep_blank_values=True))

        if params:
            tmp = {}
            for k, v in params.iteritems():
                tmp[k] = v[0]
            params = tmp

        return params


class HTTPResponse(HTTPMessage, object):
    def __init__(self, protocol, code, msg, headers=None, body=''):
        self.protocol = protocol
        self.code = code
        self.msg = msg
        self.body = body
        self.cacheable = True
        self.expiry = datetime.datetime.now() + datetime.timedelta(minutes=1)
        HTTPMessage.__init__(self, headers)

    @staticmethod
    def build(data):
        reqline = data.readline().rstrip(HTTPMessage.EOL)

        if reqline == '':
            return None

        proto, code, msg = reqline.split(None,2)

        # Read headers & body
        headers = HTTPMessage._read_headers(data)
        body = HTTPMessage._read_body(data, headers)
        return HTTPResponse(proto, code, msg, headers, body)

    def serialize(self):
        """
        Converts HTTP response method to string
        :return:
        """
        # Response line
        s = "%s %s %s" % (self.protocol, self.code, self.msg)
        s += HTTPMessage.EOL

        # Headers
        for n, v in self.headers.iteritems():
            for i in v:
                s += "%s: %s" % (n, i)
                s += HTTPMessage.EOL

        s += HTTPMessage.EOL

        # Body
        if not self.is_chunked():
            s += self.body
        else:
            s += "%x" % len(self.body) + HTTPMessage.EOL
            s += self.body + HTTPMessage.EOL
            s += HTTPMessage.EOL
            s += "0" + HTTPMessage.EOL + HTTPMessage.EOL

        return s