HTTP Proxy Server
=================

##Contributors
Murray Tannock (murray14@ru.is)


Content
-------

HTTP Proxy server for Computer Networks.
Háskolinn í Reykjavík Fall 2014.

Includes Testing client

###Specification
Server uses only standard Python 2.7.8 libraries (excluding SimpleHTTPServer, httplib, and BaseHTTPServer).
Server supports HTTP `HEAD`, `GET` and `POST` requests.
Server responds appropriately to all possible requests, superset of above.
Server should be started by running `python http_proxy.py <port_number> <logfile>`
Server should support persistent connections

###Notes
Remember to document all functions implemented using docstrings.