__author__ = 'Murray Tannock'

import httplib
import urllib


class TestClient(object):
    """
    Basic test client for HTTP/1.1 requests.
    """

    @staticmethod
    def get_status(resp):
        return resp.status

    @staticmethod
    def get_reason(resp):
        return resp.reason

    @staticmethod
    def get_headers(resp):
        return resp.getheaders()

    @staticmethod
    def get_data(resp):
        data = resp.read()
        return data

    def send_request(self, target, targetpath="/", method="GET", params=None, headers=None):
        """
        Sends constructed request to target server, and returns response.

        :param target:      targeted server, can be IP address or Domain name
        :param targetpath:  Path to requested file on server, should begin with '/'
        :param method:      HTTP method to use one of 'HEAD', 'GET', 'POST'
        :param params:      Body for post request
        :param headers:     headers for HTTP request
        :return:            response to sent request
        """
        if not headers:
            headers = {}
        conn = httplib.HTTPConnection(target)
        conn.request(method, targetpath, params, headers)
        resp = conn.getresponse()
        return resp

    def send_head_request(self, target, targetpath, headers=None):
        """
        Sends 'HEAD' request to target server and returns response.

        :param target:      targeted server, can be IP address or Domain name
        :param targetpath:  Path to requested file on server, should begin with '/'
        :param headers:     headers for HTTP request
        :return:            response to sent request
        """
        method = "HEAD"
        return self.send_request(target, targetpath, method, headers=headers)

    def send_get_request(self, target, targetpath, headers=None):
        """
        Sends 'GET' request to target server and returns response.

        :param target:      targeted server, can be IP address or Domain name
        :param targetpath:  Path to requested file on server, should begin with '/'
        :param headers:     headers for HTTP request
        :return:            response to sent request
        """
        method = "GET"
        return self.send_request(target, targetpath, method, headers=headers)

    def send_post_request(self, target, targetpath, params=None, headers=None):
        """
        Sends 'POST' request to target server with defined fields and returns response.
        :param target:      targeted server, can be IP address or Domain name
        :param targetpath:  Path to requested file on server, should begin with '/'
        :param params:      Body for post request
        :param headers:     headers for HTTP request
        :return:            response to sent request
        """
        if not headers:
            headers = {}
        if not params:
            params = {}
        method = "POST"
        params = urllib.urlencode(params)
        headers = dict({"Content-type": "application/x-www-form-urlencoded",
                        "Accept": "text/plain"}.items() + headers.items())
        return self.send_request(target, targetpath, method, params, headers)


class ProxyTestClient(TestClient, object):
    """
    Basic Test client for passing HTTP/1.1 requests through a HTTP proxy
    """

    def __init__(self, proxy_address):
        """
        Proxy information is initialised on class construction.
        :param proxy_address:   Tuple indicating proxy hostname and proxy port
        :return:                NONE
        """
        self.proxy_host = proxy_address[0]
        self.proxy_port = proxy_address[1]

    def send_request(self, target, targetpath='/', method="GET", params=None, headers=None):
        """
        Sends constructed request to target server through proxy, and returns response.

        :param target:      targeted server, can be IP address or Domain name
        :param targetpath:  Path to requested file on server, should begin with '/'
        :param method:      HTTP method to use one of 'HEAD', 'GET', 'POST'
        :param params:      Body for post request
        :param headers:     headers for HTTP request
        :return:            response to sent request
        """
        if not headers:
            headers = {}
        conn = httplib.HTTPConnection(self.proxy_host, self.proxy_port)
        conn.request(method, target + targetpath, params, headers)
        resp = conn.getresponse()
        return resp